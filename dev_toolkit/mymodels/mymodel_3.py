import torch
from torchvision import transforms
import timm
import os

from environment_setup import PROJECT_ROOT_DIR


class MyModel(torch.nn.Module):
    def __init__(self, backbone):
        super().__init__()
        self.backbone = backbone
        self.transform = transforms.Resize((224, 224))

    def forward(self, x):
        x = self.transform(x)
        x = self.backbone(x)
        x[x > 0] = 10
        x[x <= 0] = -10
        return x


class RobustModel(MyModel):
    def __init__(self):
        super().__init__(backbone=timm.create_model("ecaresnet101d_pruned", pretrained=False, num_classes=40))


if __name__ == "__main__":
    model = RobustModel()
    model.load_state_dict(torch.load(os.path.join(PROJECT_ROOT_DIR, "mymodels/SD.defence.attr.3.pth")))
    x = torch.rand(4, 3, 256, 256)
    y = model(x)
    print(y.shape)
