import torch.nn as nn
import torch.nn.functional as F
import torch
from numbers import Number
from torchvision import transforms
import timm
import os

from environment_setup import PROJECT_ROOT_DIR


class MyModel(torch.nn.Module):
    def __init__(self, backbone):
        super().__init__()
        self.backbone = backbone
        self.transform = transforms.Resize((224, 224))

    def forward(self, x):
        x = self.transform(x)
        return self.backbone(x)


class Ensemble(nn.Module):
    def __init__(self, backbone=None, mode="average"):
        super().__init__()
        if backbone is None:
            effnet = list()
            resnet = list()
            for i in range(3):
                m = timm.create_model("ecaresnet101d_pruned", pretrained=False, num_classes=40)
                resnet.append(m)
            for i in range(6):
                tf_effnet = timm.create_model("tf_efficientnet_b3_ap", pretrained=False, num_classes=40)
                effnet.append(tf_effnet)
            backbone = effnet + resnet
        self.backbone = nn.ModuleList(backbone)
        self.mode = mode

    def vote(self, scores):
        scores[scores > 0] = 1
        scores[scores <= 0] = -1
        scores = torch.sum(scores, dim=0)
        scores[scores > 0] = 10
        scores[scores <= 0] = -10
        return scores

    def average(self, scores):
        scores = scores.mean(dim=0)
        scores[scores > 0] = 10
        scores[scores <= 0] = -10
        return scores

    def forward(self, x):
        logits = list()
        for model in self.backbone:
            _logits = model(x)
            if isinstance(_logits, tuple):
                _logits = _logits[0]
            logits.append(_logits)
        logits = torch.stack(logits)
        if self.mode == "vote":
            return self.vote(logits)
        elif self.mode == "average":
            return self.average(logits)


class EnsembleDef(MyModel):
    def __init__(self):
        super().__init__(Ensemble())


if __name__ == "__main__":
    model = EnsembleDef()
    model.load_state_dict(torch.load(os.path.join(PROJECT_ROOT_DIR, "mymodels/SD.defence.attr.2.pth")))
    x = torch.rand(4, 3, 256, 256)
    y = model(x)
    print(y.shape)
